-- This is a sample virtual host

-- Define here where Xavante HTTP documents scripts are located
--
local webDir = '/usr/share/doc/xavante/'

local simplerules = {

    { -- URI remapping example
      -- here / -> /README
      -- 
      match = "^[^%./]*/$",
      with = xavante.redirecthandler,
      params = {"README"}
    }, 

    { -- filehandler example
      -- /README -> webDir/README
      --
      match = ".",
      with = xavante.filehandler,
      params = {baseDir = webDir}
    },

    -- { -- This is the  WSAPI launcher, it automatically wraps each WSAPI
    --   -- app in a (reused) ring
    --   -- remember to require 'wsapi.xavante'
    --   --
    --   match = { "%.ws$", "%.ws/"},
    --   with = wsapi.xavante.makeGenericHandler(webDir)
    -- },

    -- { -- This is a cgilua handler example
    --   -- remember to require 'xavante.cgiluahandler'
    --   --
    --   match = {"%.lp$", "%.lp/.*$", "%.lua$", "%.lua/.*$" },
    --   with = xavante.cgiluahandler.makeHandler (webDir)
    -- },
    
    -- { -- This is an orbit example
    --   -- remember to require 'orbit.ophandler'
    --   --
    --   match = {"%.op$", "%.op/.*$" },
    --   with = orbit.ophandler.makeHandler (webDir)
    -- },

} 

-- add your vhost rules to the virtualhosts map
-- 
config.virtualhosts["localhost:8080"] = {
	rules = simplerules,
}

