-- Some standard includes used by xavante
--
require "xavante"
require "xavante.filehandler"
require "xavante.redirecthandler"

-- xavante only recommends cgilua, thus this handler in not required by default
--
-- require "xavante.cgiluahandler"

-- To list the configuration files
--
require "lfs"
local conf_dir = "/etc/xavante/sites-enabled/"

-- This is the configuration table, virtual hsots should
-- fill it with rules (see the sample localhost.lua)
config = {
    server = {host = "*", port = 8080},
    defaultHost = {},
    virtualhosts = {},
}

-- load all configuration files 
for f in lfs.dir(conf_dir) do
	if string.match(f,"\.lua$") then
		local rc, err = pcall(dofile,conf_dir..f)
		if not rc then
			print('Error loading '..conf_dir..f..': '..tostring(err))
		end
	end
end

-- we also set the default host to localhost
config.defaultHost = config.virtualhosts["localhost:8080"]

-- load the configuration table
xavante.HTTP(config)

-- run xavante
xavante.start()

